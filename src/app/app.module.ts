import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './ui-layout-module/angular-material.module';
import {HeaderComponent} from './header/header.component';
import {SidemenuComponent} from './sidemenu/sidemenu.component';
import {HomeComponent} from './home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NgPrimeModule} from './ui-layout-module/ng-prime.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { CurrencyConverterComponent } from './currency-converter/currency-converter.component';
import {HttpClientModule} from '@angular/common/http';
import {ChartsModule} from "ng2-charts";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SidemenuComponent,
    HomeComponent,
    DashboardComponent,
    CurrencyConverterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgPrimeModule,
    CommonModule,
    FormsModule,
    ChartsModule,
    HttpClientModule

  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
