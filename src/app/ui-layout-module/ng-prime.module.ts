/**
 * Created by Okruti on 01-07-2020.
 */
import {NgModule} from '@angular/core';
import {DataTableModule} from 'primeng/primeng';


@NgModule({
  imports: [
    DataTableModule
  ],
  exports: [
    DataTableModule

  ]
})

export class NgPrimeModule {
}
