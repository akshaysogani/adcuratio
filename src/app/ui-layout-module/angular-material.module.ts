import {NgModule} from '@angular/core';
import {
  MatInputModule,
  MatCardModule, MatToolbarModule, MatButtonModule, MatMenuModule, MatTreeModule, MatIconModule, MatSelectModule,
  MatTableModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatTreeModule,
    MatIconModule,
    MatTableModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  exports: [
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatTreeModule,
    MatIconModule,
    MatTableModule,
    MatSelectModule,
    ReactiveFormsModule

  ]
})

export class MaterialModule {
}
