import {Component, OnInit} from '@angular/core';
import {BackendService} from '../services/backend.service';
import {ChartDataSets} from "chart.js";
import {Color, Label} from "ng2-charts";

@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.sass']
})
export class CurrencyConverterComponent implements OnInit {
  from: string;
  to: string;
  options: any;
  amount: number;
  reslutValue: string;
  lineChartData: ChartDataSets[] = [
    {data: [], label: 'Historical trend'},
  ];

  lineChartLabels: Label[];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private backendService: BackendService) {
    this.fetchData();
  }

  ngOnInit() {
  }

  // this function fetch drop down values;
  fetchData = () => {
    this.backendService.fetchCurrencyData().subscribe(
      (res: any) => {
        this.options = Object.keys(res['rates']);
        this.from = this.options[0];
        this.to = this.options[1];
        console.log(res);
      }, error => {

        console.log(error);

      });
  }

  // this function to convert value the value and display reslut in screen
  convert = () => {
    const queryParams = {
      base: this.from
    };
    this.backendService.fetchCurrencyData(queryParams).subscribe(
      (res: any) => {
        this.reslutValue = Number(res['rates'][this.to] * this.amount).toFixed(5);
        this.fetchHistoricalData();

      }, error => {

        console.log(error);

      });
  }

  // this function to use swap convert value
  swapValue() {
    const from = this.from;
    this.from = this.to;
    this.to = from;
    this.reslutValue = '';
  }

  fetchHistoricalData = () => {
    const queryParams = {
      start_at: `${new Date().getFullYear() - 1 }-${new Date().getMonth() + 1}-${new Date().getDate()}`,
      end_at: `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`,
      base: this.from
    };
    this.backendService.fetchHistoricalData(queryParams).subscribe(
      (res: any) => {
        const labels: any = Object.keys(res['rates']);
        labels.sort((a: any, b: any) => {
          // Turn your strings into dates, and then subtract them
          // to get a value that is either negative, positive, or zero.const x: any = new Date(b);
          const y: any = new Date(a);
          const x: any = new Date(b);
          return x - y;
        });
        const data = labels.map(lable => {
          console.log(lable);
          return res['rates'][lable][this.to] * this.amount;
        });
        this.lineChartLabels = labels;
        this.lineChartData = [{
          data: data,
          label: 'Historical trend'
        }];
      }, error => {

        console.log(error);

      });
  }
}
