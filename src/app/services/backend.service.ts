import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) {
  }

  fetchCurrencyData(queryParams?) {
    return this.http.get('https://api.exchangeratesapi.io/latest', {'params': queryParams});

  }

  fetchHistoricalData(queryParams) {
    return this.http.get('https://api.exchangeratesapi.io/history', {'params': queryParams});

  }
}
